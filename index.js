// S47 Activity JS DOM Manipulation
// console.log("hello world");

// "document" refers to the whole webpage
// "querySelector" is used to select a specific object (HTML element) from our document (webpage)
const txtFirstName = document.querySelector("#txt-first-name");
const txtLastName = document.querySelector("#txt-last-name");
const spanFullName = document.querySelector("#span-full-name");

// document.getElementByID()
// document.getElementByClassName()
// const txtFirstName = document.getElementByID("txt-first-name");
// const spanFullName = document.getElementByID("span-full-name");

// Whenever a user interacts with a web page, this action is considred as an event
// "addEventListener" taskes two argument
// "keyup" - string identifying an event
// function that the listener will execute once the specified event is triggered
txtFirstName.addEventListener('keyup', (event) => {
	// "innerHTML" property sets or returns the HTML content
	const fullName = txtFirstName.value + ' ' + txtLastName.value;
	spanFullName.innerHTML = fullName;
	
	// "event.target" contains the element where the event happened
	console.log(event.target);
	// "event.target.value" gets the value of the input object
	console.log(event.target.value);
});

txtLastName.addEventListener('keyup', (event) => {	
	const fullName = txtFirstName.value + ' ' + txtLastName.value;
	spanFullName.innerHTML = fullName;	
	
	console.log(event.target);	
	console.log(event.target.value);
});


